package TTYUtil;

use 5.006;
use strict;
use warnings;

{
	our $VERSION = '0.01';
}

eval
{
	use Term::ANSIColor qw/:pushpop/
};

use Carp qw(:DEFAULT cluck);
use Exporter 'import';
use Module::Loaded; # is_loaded()


our @EXPORT = qw();
our @EXPORT_OK = qw/
					echo
					note
					quote
					res
					danger
					emphasis
					exec_r_cmd
					exec_rw_cmd
					decode_json_file
					/;

our %EXPORT_TAGS = ();	
our %_OPT;

BEGIN
{
	my @mode_names = qw/
						markdown_mode
						execute_mode
						quiet_mode
						colored_echo
						/;

	for my $mode_name ( @mode_names )
	{
		no strict 'refs';

		my $getter_fullname = sprintf(
								'%s::get_%s'
								,__PACKAGE__
								,$mode_name
								);
		my $setter_fullname = sprintf(
								'%s::set_%s'
								,__PACKAGE__
								,$mode_name
								);

		if( ! defined &{$getter_fullname} )
		{
			*{$getter_fullname} = sub
			{
				return __PACKAGE__->get_mode_for( $mode_name );
			}
		}

		if( ! defined &{$setter_fullname} )
		{
			*{$setter_fullname} = sub
			{
				return __PACKAGE__->set_mode_for( $mode_name , $_[1] );
			}
		}
	}
}

INIT
{
	my $_ansicolor_loaded = undef;

	sub ANSIColorLoaded
	{
		if( ! defined $_ansicolor_loaded )
		{
			$_ansicolor_loaded = is_loaded( 'Term::ANSIColor' ) ? 1 : 0;
		}

		return $_ansicolor_loaded;
	}

	__PACKAGE__->set_markdown_mode( 0 );
	__PACKAGE__->set_execute_mode( 0 );
	__PACKAGE__->set_colored_echo( 0 );
	__PACKAGE__->set_quiet_mode( 0 );

	sub _to_markdown_lines
	{
		return map{ m/(?:^\s*[\-\s])|(?:^\s*\d+\.\s)/ ? $_ : $_ ."\n" } @_;
	}
}


sub echo(@)
{
	print STDERR "\n" if( __PACKAGE__->get_markdown_mode );

	if( ANSIColorLoaded &&  __PACKAGE__->get_colored_echo )
	{
		print STDOUT LOCALCOLOR( BLUE @_,"\n" );
	}
	else
	{
		print STDOUT @_,"\n";
	}
}

sub note(@)
{
	return if( __PACKAGE__->get_quiet_mode );

	@_ = _to_markdown_lines( @_ ) if( $_OPT{markdown_mode} );

	if( ANSIColorLoaded )
	{
		map { print STDERR LOCALCOLOR GREEN $_ } @_;
		print STDERR "\n";
	}
	else
	{
		print STDERR @_;
		print STDERR "\n";
	}
}

sub danger(@)
{
	return if( __PACKAGE__->get_quiet_mode );

	if( $_OPT{markdown_mode} )
	{
		@_ = _to_markdown_lines( @_ );
	}

	if( ANSIColorLoaded )
	{
		print STDERR LOCALCOLOR( RED @_,"\n" );
	}
	else
	{
		print STDERR @_,"\n";
	}
}

sub quote(@)
{
	return if( __PACKAGE__->get_quiet_mode );

	if( $_OPT{markdown_mode} )
	{
		@_ = map { $_ =~ s/^\s*```/'''/g } @_;

		unshift @_ ,'```';
		push @_ ,'```';
	}
	else
	{
		@_ = map { $_ = '  '.$_ } @_;
	}

	if( ANSIColorLoaded )
	{
		print STDERR LOCALCOLOR( DARK @_,"\n" );
	}
	else
	{
		print STDERR @_ , "\n";
	}

}

sub res(@)
{
	return if( __PACKAGE__->get_quiet_mode );

	my $buf = join("\n" , map{ $_ //= '' } @_ );

	for my $line ( split(/\r\n|\r|\n/, $buf ) )
	{
		# $_ =~ s/(\r\n|\r|\n)/\n>/g;
		# $_ //= '';
		if( ANSIColorLoaded )
		{
			print STDERR LOCALCOLOR( DARK '> '.$line."\n" );
		}
		else
		{
			print STDERR '> '.$line."\n";
		}
		
	}
}


sub exec_r_cmd
{
	return _exec_cmd( $_[0] , 0 );
}

sub exec_rw_cmd
{
	return _exec_cmd( $_[0] , 1 );
}

sub _exec_cmd
{
	my $cmd = shift @_;
	my $exec_if_exec_mode = shift @_ // 1;

	if( $_OPT{markdown_mode} )
	{
		note '[CMD] : `'.$cmd.'`';
	}
	else
	{
		note '[CMD] : '.$cmd;
	}

	no warnings qw/uninitialized/;
	$? = undef;
	use warnings qw/uninitialized/;

	my $exec = sub
	{
		my $reuslt = `$cmd`;

		quote $reuslt;

		if( $? )
		{
			warn "'$cmd' returns exit code ".($? >> 8 );
		}

		return $reuslt;
	};

	my $dry_run = sub
	{
		note '  >> not execute mode.';
		return '';
	};

	if( $exec_if_exec_mode )
	{
		if( __PACKAGE__->get_execute_mode )
		{
			return $exec->();
		}
		else
		{
			return $dry_run->();
		}
	}
	else
	{
		return $exec->();
	}
}

# sub set_markdown_mode
# {
# 	return __PACKAGE__->set_mode_for( 'markdown_mode',$_[1] )
# }

# sub get_markdown_mode
# {
# 	return __PACKAGE__->get_mode_for( 'markdown_mode' );
# }

# sub set_execute_mode
# {
# 	my $class = shift @_;

# 	$_OPT{execute} = 1 && shift @_;

# 	return __PACKAGE__->get_execute_mode;
# }

# sub get_execute_mode
# {
# 	my $class = shift @_;

# 	return $_OPT{execute};
# }

# sub set_quiet_mode
# {
# 	return __PACKAGE__->set_mode_for('quiet' , 1 && $_[1] );
# }

# sub get_quiet_mode
# {
# 	return __PACKAGE__->get_mode_for('quiet');
# }

sub set_mode_for
{
	my $class	= shift @_;
	my $key		= shift @_;
	my $value	= shift @_;

	$_OPT{$key} = $value;

	return __PACKAGE__->get_mode_for( $key );
}

sub get_mode_for
{
	my $class = shift @_;

	return $_OPT{$_[0]};
}

1;

# subtree merge 本家側コミットで subtree merget した方がどうなるか実験するための modify 作成コメント

__END__

=head1 NAME

TTYUtil - The great new TTYUtil!

=head1 VERSION

Version 0.01

=cut




=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use TTYUtil qw/
    				echo
    				note
    				danger
    				emphasis
    			/;

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 function1

=cut

sub function1 {
}

=head2 function2

=cut

sub function2 {
}

=head1 AUTHOR

H.Seo, C<< <nullcarrier at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-ttyutil at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=TTYUtil>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc TTYUtil


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=TTYUtil>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/TTYUtil>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/TTYUtil>

=item * Search CPAN

L<http://search.cpan.org/dist/TTYUtil/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2017 H.Seo.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of TTYUtil
