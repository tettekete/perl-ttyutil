=pod

=encoding utf8

=head1 SUMMARY

Unit Test for TTYUtil
since 2017/08/17 by H.Seo

=head1 USAGE

case1:

```
$ cd <above-your-lib>
$ perl -Ilib <path-to-this-script>
```


=cut

use strict;
use warnings;

use Test::More;
use Test::Exception;
BEGIN
{
	eval 'use Test::More::Color "foreground"';
}

use Test::Output;
use Capture::Tiny qw/capture/;

use Data::Dumper;
$Data::Dumper::Terse	= 1;
$Data::Dumper::Sortkeys	= 1;
$Data::Dumper::Indent	= 2;

{
	BEGIN
	{
		BAIL_OUT("use TTYUtil faild.") unless use_ok('TTYUtil',qw/note/);
	}
	
	ok( ! TTYUtil->set_markdown_mode( 0 ) ,'! TTYUtil->set_markdown_mode( 0 )' );
	ok( ! TTYUtil->get_markdown_mode() ,'! TTYUtil->get_markdown_mode()' );

	my $esc = "\033".'\[';

	output_like(
				sub { note "Note message." }
				,qr//
				,qr/$esc.+?Note message\..+\n/
				,'note on no markdown_mode'
			);

	# markdonw_mode のテスト
	{
		ok( TTYUtil->set_markdown_mode( 1 ) ,'TTYUtil->set_markdown_mode( 1 )' );
		ok( TTYUtil->get_markdown_mode() ,'TTYUtil->get_markdown_mode()' );

		my ($std ,$err ) = capture { note "Note message." };

		ok( ! $std );
		unless(
			like( $err
				,qr/$esc.+?Note message\..*\n.*\n/
				,'note on no markdown_mode'
			)
			# カラーエスケープシーケンスの終了箇所は曖昧（`Term::ANSIColor` 依存なのと、あまり気にしていないのと）。
		)
		{
			diag dump_like_hex_editor( $err );
		}

		# TODO:
		# markdown モードの時リスト渡しでアウトライン系の入力内容の場合余計な改行を入れない様にしたいが
		# 仮実装状態で未テスト
	}

	done_testing();
}


sub dump_like_hex_editor
{
	my $bytes           = shift @_;
	my $bytesPerLine    = shift @_ || 16;

	no warnings 'uninitialized';

	my $buf = '';
	my $address = 0;

	while( $bytes =~ /(.{1,$bytesPerLine})/gs )
	{
		my $seg = $1;

		my $hex = join(' ' , map{unpack('H2',$_)} ($seg =~ /([\x00-\xFF])/g));
		my $ascii = $seg;
		$ascii =~ s/[\x00-\x1f]|[\x7f-\xff]/./g;

		$buf .= sprintf("%04d : %-".(3 * $bytesPerLine)."s : %s\n"
							,$address
							,$hex
							,$ascii);

		$address += $bytesPerLine;
	}

	return $buf;
}
