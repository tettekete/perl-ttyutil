=pod

=encoding utf8

=head1 SUMMARY

Unit Test for TTYUtil
since 2017/08/17 by H.Seo

=head1 USAGE

case1:

```
$ cd <here>
$ perl -I../lib <this_script_name>
```

case2:

```
$ cd <parent dir>
$ prove -lc t/<this_script_name>
# l: Add 'lib' to the path for your tests (-Ilib).
# c: Colored test output (default).
```

=cut

use strict;
use warnings;

use Test::More;
use Test::Exception;
eval 'use Test::More::Color "foreground"';
use Test::Output;


{
	my $fail = 0;

	BEGIN
	{
		BAIL_OUT("use TTYUtil faild.") unless use_ok('TTYUtil',qw/echo/);
	}
	
	stdout_is( sub{ echo "hoge" }
				,"hoge\n"
				,"echo with no markdown mode."
				);

	TTYUtil->set_markdown_mode( 1 );

	ok( TTYUtil->set_markdown_mode( 1 ) ,'TTYUtil->set_markdown_mode( 1 )' );
	ok( TTYUtil->get_markdown_mode() ,'TTYUtil->get_markdown_mode()' );

	output_is( sub{ echo "hoge" }
				,"hoge\n"
				,"\n"
				,"echo with markdown mode."
				);


	done_testing();
}

